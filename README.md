# group16-melbourne-pollen

Project for group 16 Master of Data Science major project on Melbourne Pollen

Folder contents:
- Meeting-Notes: contains our internal and client meeting Notes
- Modelling: contains our models for pollen forecast
- feature_analysis: contains code for preliminary feature analysis of weather variables.
- gfs: contains code for retrieving real-time and archived gfs Data
- preprocessing: contains code for the preprocessing of the data available to us.
